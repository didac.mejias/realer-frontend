export interface UserCredentials {
  email: string;
  password: string;
}

export interface NewUser extends UserCredentials {
  nickname: string;
}
