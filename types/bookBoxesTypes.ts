export interface IBookBox {
  id: string;
  title: string;
  description: string;
  portrait: string;
  language: string;
  genre: string[];
  author: string;
  created: string;
}
