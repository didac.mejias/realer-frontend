import dynamic from 'next/dynamic';

const Feed = dynamic(() => import('@modules-feed/Feed'));
const Landing = dynamic(() => import('@modules-landing/Landing'));

export default Feed;
