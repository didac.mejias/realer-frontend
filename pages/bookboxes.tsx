import dynamic from 'next/dynamic';

const Layout = dynamic(() => import('@modules-common/Layout'));

const Bookboxes = () => {
  return (
    <>
      <Layout>
        <h1 className="text-2xl font-light text-gray-600">Bookboxes</h1>
      </Layout>
    </>
  );
};

export default Bookboxes;
