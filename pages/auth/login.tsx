import dynamic from 'next/dynamic';

const Login = dynamic(() => import('@modules-auth/login/Login'));

export default Login;
