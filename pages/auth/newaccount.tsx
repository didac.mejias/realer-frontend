import dynamic from 'next/dynamic';

const Signup = dynamic(() => import('@modules-auth/signup/Signup'));

export default Signup;
