export default {
  index: '/',
  bookbox: '/bookbox',
  bookboxes: '/bookboxes',
  login: '/auth/login',
  newaccount: '/auth/newaccount'
};
