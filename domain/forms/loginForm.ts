import * as Yup from 'yup';

export const initialValues = {
  email: '',
  password: ''
};

export const validationSchema = Yup.object({
  email: Yup.string()
    .email('Email is not valid')
    .required('Email is required'),
  password: Yup.string()
    .required('Password is required')
    .min(6, 'Password must have at least 6 characters')
});
