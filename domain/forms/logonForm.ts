import * as Yup from 'yup';

export const initialValues = {
  nickname: '',
  email: '',
  password: ''
};

const user = {
  nickname: Yup.string().required('Nickname is required'),
  email: Yup.string()
    .email('Email is not valid')
    .required('Email is required'),
  password: Yup.string()
    .required('Password is required')
    .min(6, 'Password must have at least 6 characters')
};

export const validationSchema = Yup.object(user);
