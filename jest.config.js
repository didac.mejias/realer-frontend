module.exports = {
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules/**'
  ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },
  setupFilesAfterEnv: ['./setupTests.js'],
  preset: 'ts-jest',
  testPathIgnorePatterns: ['/node_modules/', '/.next/'],
  transform: {
    '^.+\\.(js|jsx)$': './node_modules/babel-jest',
    '^.+\\.css$': './config/jest/cssTransform.js',
    '^.+\\.(ts|tsx)?$': 'babel-jest'
  },
  transformIgnorePatterns: [
    '/node_modules/',
    '^.+\\.module\\.(css|sass|scss)$'
  ],
  testRegex: '(/__test__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
  testURL: 'http://localhost',
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
    '@domain/(.*)$': './domain/$1',
    '@components/(.*)$': './components/$1',
    '@pages/(.*)$': './pages/$1',
    '@services/(.*)$': './services/$1',
    '@config/(.*)$': './config/$1',
    '@types/(.*)$': './types/$1',
    '@modules-common/(.*)$': './modules/common/$1',
    '@modules-auth/(.*)$': './modules/auth/$1',
    '@modules-feed/(.*)$': './modules/feed/$1',
    '@modules-landing/(.*)$': './modules/landing/$1'
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  snapshotSerializers: ['enzyme-to-json/serializer']
};
