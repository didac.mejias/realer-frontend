import { gql } from '@apollo/client';

export const NEW_ACCOUNT_QUERY = gql`
  mutation createUser($input: UserInput) {
    createUser(input: $input) {
      id
      nickname
      email
    }
  }
`;

export const LOGIN_QUERY = gql`
  mutation authenticate($input: AuthenticateUser) {
    authenticate(input: $input) {
      token
    }
  }
`;

export const GET_LOGGED_USER = gql`
  query getUser {
    getUser {
      id
      nickname
      email
      created
    }
  }
`;
