import { gql } from '@apollo/client';

export const GET_ALL_BOOKBOXES_QUERY = gql`
  query getAllBookBoxes {
    getAllBookBoxes {
      id
      title
      description
      portrait
      language
      genre
      author
      created
    }
  }
`;
