import { ReactElement, ReactNode, FC } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import PATHNAMES from '@domain/pathnames';
import dynamic from 'next/dynamic';

const LoggedPage = dynamic(() => import('@components/Loggedpage'));
const BasicPage = dynamic(() => import('@components/Basicpage'));

const Layout: FC = ({ children }: ReactNode): ReactElement => {
  const router = useRouter();
  const path = router?.pathname;
  const isAuth = path === PATHNAMES.login || path === PATHNAMES.newaccount;

  return (
    <>
      <Head>
        <title>Krubk</title>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css"
          integrity="sha512-oHDEc8Xed4hiW6CxD7qjbnI+B07vDdX7hEPTvn9pSZO1bcRqHp8mj9pyr+8RVC2GmtEfI2Bi9Ke9Ass0as+zpg=="
          crossOrigin="anonymous"
        />
        <link
          href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
      </Head>
      {isAuth ? (
        <BasicPage>{children}</BasicPage>
      ) : (
        <LoggedPage>{children}</LoggedPage>
      )}
    </>
  );
};

export default Layout;
