import React, { ReactElement, useState } from 'react';
import dynamic from 'next/dynamic';

const LoginForm = dynamic(() => import('@components/form/LoginForm'));
const Layout = dynamic(() => import('@modules-common/Layout'));
const FormMessage = dynamic(() =>
  import('@components/form/components/FormMessage')
);

const Login: React.FC = (): ReactElement => {
  const [message, setMessage] = useState<string | null>(null);

  const showMessage = (): ReactElement => {
    return <FormMessage message={message} />;
  };

  const setErrorMessage = (ErrorMessage: string): void => {
    setMessage(ErrorMessage);
  };

  return (
    <Layout>
      {message && showMessage()}
      <h1 className="text-2xl font-light text-center text-white">Login</h1>
      <div className="flex justify-center mt-5">
        <div className="w-full max-w-sm">
          <LoginForm setErrorMessage={setErrorMessage} />
        </div>
      </div>
    </Layout>
  );
};

export default Login;
