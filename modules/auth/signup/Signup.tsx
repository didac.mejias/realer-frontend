import React, { ReactElement, useState } from 'react';
import dynamic from 'next/dynamic';

const NewAccountForm = dynamic(() => import('@components/form/NewAccountForm'));
const FormMessage = dynamic(() =>
  import('@components/form/components/FormMessage')
);
const Layout = dynamic(() => import('@modules-common/Layout'));

const Signup: React.FC = (): ReactElement => {
  const [message, setMessage] = useState<string | null>(null);

  const showMessage = (): ReactElement => {
    return <FormMessage message={message} />;
  };

  const setErrorMessage = (ErrorMessage: string): void => {
    setMessage(ErrorMessage);
  };

  return (
    <Layout>
      {message && showMessage()}
      <h1 className="text-2xl font-light text-white text-center">
        New account
      </h1>
      <div className="flex justify-center mt-5">
        <div className="w-full max-w-sm">
          <NewAccountForm setErrorMessage={setErrorMessage} />
        </div>
      </div>
    </Layout>
  );
};

export default Signup;
