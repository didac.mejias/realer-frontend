import React, { ReactElement } from 'react';

const Landing: React.FC = (): ReactElement => {
  return <h1>Landing page</h1>;
};

export default Landing;
