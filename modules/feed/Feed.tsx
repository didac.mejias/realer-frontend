import React, { ReactElement } from 'react';
import dynamic from 'next/dynamic';
import { useQuery } from '@apollo/client';
import { GET_ALL_BOOKBOXES_QUERY } from '@services/auth/bookboxesService';
import BookBox from '@modules-feed/components/BookBox';
import { IBookBox } from '@types/bookBoxesTypes';

const Layout = dynamic(() => import('@modules-common/Layout'));

const Feed: React.FC = (): ReactElement => {
  const { data: { getAllBookBoxes: bookBoxes = [] } = {} } = useQuery(
    GET_ALL_BOOKBOXES_QUERY
  );

  return (
    <>
      <Layout>
        <h1 className="text-2xl font-light text-gray-600">Index</h1>
        <div className="grid grid-cols-1 xl:grid-cols-3 gap-4">
          {bookBoxes.map((bookbox: IBookBox) => (
            <BookBox key={bookbox.id} bookbox={bookbox} />
          ))}
        </div>
      </Layout>
    </>
  );
};

export default Feed;
