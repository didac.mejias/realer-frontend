import React, { ReactElement } from 'react';
import { IBookBox } from '@types/bookBoxesTypes';

const BookBox: React.FC = ({ bookbox }): ReactElement => {
  const {
    portrait,
    title,
    description,
    created,
    genre: genres
  }: IBookBox = bookbox;

  return (
    <article id="bookbox">
      <div className="max-w-md w-full">
        <div
          className="h-48 flex-none bg-cover rounded-t text-center overflow-hidden"
          style={{ backgroundImage: `url('${portrait}')` }}
        />
        <div>
          <h2>{title}</h2>
          <small>{created}</small>
          <p>{description}</p>
        </div>
        <div>
          <small>{genres && genres.map((genre: string) => genre)}</small>
        </div>
      </div>
    </article>
  );
};

export default BookBox;
