import { ReactNode } from 'react';

type BasicpageProps = {
  children: ReactNode;
};

const Basicpage = ({ children }: BasicpageProps) => {
  return (
    <main className="bg-gray-700 min-h-screen flex flex-col justify-center">
      {children}
    </main>
  );
};

export default Basicpage;
