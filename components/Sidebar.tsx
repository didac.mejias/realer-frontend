import Link from 'next/link';
import { useRouter } from 'next/router';
import PATHNAME from '@domain/pathnames';

const Sidebar = () => {
  const router = useRouter();
  const path = router?.pathname;

  return (
    <aside className="bg-gray-500 sm:w-1/3 xl:w-1/5 sm:min-h-screen p-5">
      <p className="text-white text-2xl font-black">Sidebar</p>
      <nav className="mt-5 list-none">
        <li className={path === PATHNAME.index ? 'bg-blue-800 p-3' : 'p-3'}>
          <Link href={PATHNAME.index}>
            <a className="mb-2 block">Index</a>
          </Link>
        </li>
        <li className={path === PATHNAME.bookboxes ? 'bg-blue-800 p-3' : 'p-3'}>
          <Link href={PATHNAME.bookboxes}>
            <a className="mb-2 block">Bookboxes</a>
          </Link>
        </li>
        <li className={path === PATHNAME.bookbox ? 'bg-blue-800 p-3' : 'p-3'}>
          <Link href={PATHNAME.bookbox}>
            <a className="mb-2 block">Bookbox</a>
          </Link>
        </li>
      </nav>
    </aside>
  );
};

export default Sidebar;
