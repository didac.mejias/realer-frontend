import dynamic from 'next/dynamic';

import { ReactNode } from 'react';

const Sidebar = dynamic(() => import('@components/Sidebar'));

type LoggedPageProps = {
  children: ReactNode;
};

const LoggedPage = ({ children }: LoggedPageProps) => {
  return (
    <div className="flex bg-gray-100 min-h-screen">
      <Sidebar />
      <main className="sm:w-2/3 xl:w-4/5 sm:min-h-screen p-5">{children}</main>
    </div>
  );
};

export default LoggedPage;
