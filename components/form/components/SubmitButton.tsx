type SubmitProps = {
  value: string;
};

const SubmitButton = ({ value }: SubmitProps) => {
  return (
    <input
      className="bg-gray-800 w-full mt-5 p-2 text-white uppercase hover:bg-gray-900"
      type="submit"
      value={value}
    />
  );
};

export default SubmitButton;
