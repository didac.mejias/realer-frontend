type Message = {
  message: string | null;
};

const FormMessage = ({ message }: Message) => {
  return (
    <div className="bg-white py-2 px-3 w-full max-w-sm text-center mx-auto my-3">
      <p>{message}</p>
    </div>
  );
};

export default FormMessage;
