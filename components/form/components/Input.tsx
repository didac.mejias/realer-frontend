import { ChangeEvent } from 'react';

type InputProps = {
  id: string;
  type: string;
  placeholder: string;
  value: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onBlur: (event: ChangeEvent<HTMLInputElement>) => void;
  errorMessage: string;
};

const Input = (props: InputProps) => {
  const {
    id,
    type,
    placeholder,
    value,
    onChange,
    onBlur,
    errorMessage
  } = props;

  return (
    <div>
      <input
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        id={id}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
      <div className="my-2 bg-red-100 px-2 border-l-4 border-red-500 text-red-700 font-bold">
        {errorMessage}
      </div>
    </div>
  );
};

export default Input;
