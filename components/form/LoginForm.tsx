import dynamic from 'next/dynamic';
import { Formik } from 'formik';
import { initialValues, validationSchema } from '@domain/forms/loginForm';
import { useMutation } from '@apollo/client';
import { LOGIN_QUERY } from '@services/auth/authService';
import { useRouter } from 'next/router';
import { UserCredentials } from '@types/userType';

const Input = dynamic(() => import('@components/form/components/Input'));
const Label = dynamic(() => import('@components/form/components/Label'));
const SubmitButton = dynamic(() =>
  import('@components/form/components/SubmitButton')
);

const LoginForm = ({ setErrorMessage }: any) => {
  const [authenticate] = useMutation(LOGIN_QUERY);
  const router = useRouter();

  const handleSubmit = async (values: UserCredentials): Promise<void> => {
    try {
      const {
        data: {
          authenticate: { token }
        }
      } = await authenticate({
        variables: {
          input: {
            ...values
          }
        }
      });

      setErrorMessage('Authenticating...');
      localStorage.setItem('token', token);
      setErrorMessage(null);
      router.push('/');
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {props => (
        <form
          onSubmit={props.handleSubmit}
          className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
        >
          <div className="mb-4">
            <Label htmlFor="email">Email</Label>
            <Input
              id="email"
              type="email"
              placeholder="example@example.com"
              value={props.values?.email}
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              errorMessage={props.touched?.email && props.errors?.email}
            />
          </div>
          <div className="mb-4">
            <Label htmlFor="password">Password</Label>
            <Input
              id="password"
              type="password"
              placeholder="password"
              value={props.values?.password}
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              errorMessage={props.touched?.password && props.errors?.password}
            />
          </div>
          <SubmitButton value="Login" />
        </form>
      )}
    </Formik>
  );
};

export default LoginForm;
