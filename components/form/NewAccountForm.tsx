import dynamic from 'next/dynamic';
import { Formik } from 'formik';
import { initialValues, validationSchema } from '@domain/forms/logonForm';
import { useRouter } from 'next/router';
import { useMutation } from '@apollo/client';
import { NEW_ACCOUNT_QUERY } from '@services/auth/authService';
import { NewUser } from '@types/userType';

const Input = dynamic(() => import('@components/form/components/Input'));
const Label = dynamic(() => import('@components/form/components/Label'));
const SubmitButton = dynamic(() =>
  import('@components/form/components/SubmitButton')
);

const NewAccountForm = ({ setErrorMessage }: any) => {
  const [createUser] = useMutation(NEW_ACCOUNT_QUERY);
  const router = useRouter();
  const handleSubmit = async (values: NewUser): Promise<void> => {
    try {
      await createUser({
        variables: {
          input: {
            ...values
          }
        }
      });

      setErrorMessage('User created correctly');
      setErrorMessage('');
      router.push('/auth/login');
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {props => (
        <form
          onSubmit={props.handleSubmit}
          className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
        >
          <div className="mb-4">
            <Label htmlFor="nickname">Nickname</Label>
            <Input
              id="nickname"
              type="text"
              placeholder="johndoe"
              value={props.values?.nickname}
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              errorMessage={props.touched?.nickname && props.errors?.nickname}
            />
          </div>
          <div className="mb-4">
            <Label htmlFor="email">Email</Label>
            <Input
              id="email"
              type="email"
              placeholder="example@example.com"
              value={props.values?.email}
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              errorMessage={props.touched?.email && props.errors?.email}
            />
          </div>
          <div className="mb-4">
            <Label htmlFor="password">Password</Label>
            <Input
              id="password"
              type="password"
              placeholder="password"
              value={props.values?.password}
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              errorMessage={props.touched?.password && props.errors?.password}
            />
          </div>
          <SubmitButton value="Create new account" />
        </form>
      )}
    </Formik>
  );
};

export default NewAccountForm;
