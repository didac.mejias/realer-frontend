import Index from '@pages/index';
import { shallow } from 'enzyme';

describe('Index should', () => {
  const wrapper = shallow(<Index />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
