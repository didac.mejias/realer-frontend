import Bookbox from '@pages/bookbox';
import { shallow } from 'enzyme';

describe('Bookbox should', () => {
  const wrapper = shallow(<Bookbox />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
