import Bookboxes from '@pages/bookboxes';
import { shallow } from 'enzyme';

describe('Bookboxes should', () => {
  const wrapper = shallow(<Bookboxes />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
