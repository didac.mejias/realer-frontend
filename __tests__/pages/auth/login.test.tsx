import Login from '@pages/auth/login';
import { shallow } from 'enzyme';

describe('Login should', () => {
  const wrapper = shallow(<Login />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
