import { shallow } from 'enzyme';
import NewAccount from '@pages/auth/login';

describe('NewAccount should', () => {
  const wrapper = shallow(<NewAccount />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
