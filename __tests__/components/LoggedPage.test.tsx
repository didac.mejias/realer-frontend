import LoggedPage from '@components/Loggedpage';
import { shallow } from 'enzyme';

describe('LoggedPage should', () => {
  const wrapper = shallow(
    <LoggedPage>
      <h1>Test</h1>
    </LoggedPage>
  );

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
