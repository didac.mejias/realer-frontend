import Sidebar from '@components/Sidebar';
import { shallow } from 'enzyme';

describe('Sidebar should', () => {
  const wrapper = shallow(<Sidebar />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
