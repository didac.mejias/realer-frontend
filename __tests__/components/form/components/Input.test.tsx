import Input from '@components/form/components/Input';
import { shallow } from 'enzyme';

describe('Input should', () => {
  const inputInfo = {
    id: 1,
    text: 'test',
    doSomething: jest.fn()
  };
  const wrapper = shallow(
    <Input
      id={inputInfo.id}
      type={inputInfo.text}
      placeholder={inputInfo.text}
      value={inputInfo.text}
      onChange={inputInfo.doSomething}
      onBlur={inputInfo.doSomething}
      errorMessage={inputInfo.text}
    />
  );

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
