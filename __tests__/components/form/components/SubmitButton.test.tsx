import SubmitButton from '@components/form/components/SubmitButton';
import { shallow } from 'enzyme';

describe('SubmitButton should', () => {
  const wrapper = shallow(<SubmitButton value="submit" />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
