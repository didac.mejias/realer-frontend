import FormMessage from '@components/form/components/FormMessage';
import { shallow } from 'enzyme';

describe('FormMessage should', () => {
  const wrapper = shallow(<FormMessage message="error" />);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
