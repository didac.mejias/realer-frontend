import Label from '@components/form/components/Label';
import { shallow } from 'enzyme';

describe('Label should', () => {
  const wrapper = shallow(<Label htmlFor="test">Test</Label>);

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
