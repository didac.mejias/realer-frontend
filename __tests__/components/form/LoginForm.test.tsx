import LoginForm from '@components/form/LoginForm';
import { shallow } from 'enzyme';
import { MockedProvider } from '@apollo/client/testing';
import { LOGIN_QUERY } from '@services/auth/authService';

describe('LoginForm should', () => {
  const setErrorMessage = jest.fn();
  const mocks = [
    {
      request: {
        query: LOGIN_QUERY,
        variables: {
          email: 'test@test.com',
          password: '123456'
        }
      },
      result: {
        data: {
          dog: {
            id: '1',

            email: 'test@test.com',
            password: '123456'
          }
        }
      }
    }
  ];
  const wrapper = shallow(
    <MockedProvider mocks={mocks} addTypename={false}>
      <LoginForm setErrorMessage={setErrorMessage} />
    </MockedProvider>
  );

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
