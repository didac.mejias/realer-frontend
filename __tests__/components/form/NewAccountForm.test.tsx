import NewAccountForm from '@components/form/NewAccountForm';
import { shallow } from 'enzyme';
import { MockedProvider } from '@apollo/client/testing';
import { NEW_ACCOUNT_QUERY } from '@services/auth/authService';

describe('NewAccountForm should', () => {
  const setErrorMessage = jest.fn();
  const mocks = [
    {
      request: {
        query: NEW_ACCOUNT_QUERY,
        variables: {
          nickname: 'John Doe',
          email: 'test@test.com',
          password: '123456'
        }
      },
      result: {
        data: {
          dog: {
            id: '1',
            nickname: 'John Doe',
            email: 'test@test.com',
            password: '123456'
          }
        }
      }
    }
  ];
  const wrapper = shallow(
    <MockedProvider mocks={mocks} addTypename={false}>
      <NewAccountForm setErrorMessage={setErrorMessage} />
    </MockedProvider>
  );

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
