import { shallow } from 'enzyme';
import Basicpage from '@components/Basicpage';

describe('BasicPage should', () => {
  const wrapper = shallow(
    <Basicpage>
      <h1>Test</h1>
    </Basicpage>
  );

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
