import { shallow } from 'enzyme';
import Layout from '@modules-common/Layout';

describe('Layout should', () => {
  const wrapper = shallow(
    <Layout>
      <h1>Test</h1>
    </Layout>
  );

  test('Match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
