module.exports = {
  env: {
    browser: true,
    node: true,
    es2020: true,
    es6: true,
    commonjs: true,
    "jest/globals": true,
  },
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 8,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
      arrowFunctions: true,
      binaryLiterals: true,
      blockBindings: true,
      classes: true,
    },
  },
  plugins: ["@typescript-eslint", "react"],
  extends: [
    "airbnb",
    "airbnb/hooks",
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:import/typescript",
    "plugin:jest/recommended",
    "nextjs",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended"
  ],
  rules: {
    '@typescript-eslint/indent': ['error', 2],
    '@typescript-eslint/quotes': ["error", "single"],
    semi: ["error", "always"],
    "@typescript-eslint/space-before-function-paren": ["error", "always"],
    "object-curly-spacing": ["error", "always"],
    "key-spacing": ["error", { beforeColon: false }],
    "keyword-spacing": ["error", { before: true }],
    "padding-line-between-statements": [
      "error",
      { blankLine: "always", prev: "*", next: "return" },
      { blankLine: "always", prev: ["const", "let", "var"], next: "*" },
      {
        blankLine: "any",
        prev: ["const", "let", "var"],
        next: ["const", "let", "var"],
      },
    ],
    "comma-dangle": ["error", "never"],
    "no-trailing-spaces": "error",
    'react/jsx-filename-extension': [2, { 'extensions': ['.js', '.jsx', '.ts', '.tsx'] }],
    "no-use-before-define": "off",
    "@typescript-eslint/no-use-before-define": ["error"]
  },
  settings: {
    "import/resolver": {
      "babel-module": {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
      },
      node: {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
        paths: ["src"],
      },
    },
  },
};
